<?php

namespace Cbenjafield\Albert;

use Illuminate\Support\Facades\Facade;

class BertieFacade extends Facade
{

	protected static function getFacadeAccessor()
	{
		return 'bertie';
	}

}