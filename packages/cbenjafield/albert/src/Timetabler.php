<?php

namespace Cbenjafield\Albert;

use App\Allocation;
use DB;

trait Timetabler
{

	use GenerateTimetable;

	/**
	 * Checks whether the specified day/time is available
	 * to have an allocation assigned to it or if slot
	 * is already taken.
	 *
	 * @param int $day_id
	 * @param int $room_id
	 * @param string $start_time
	 * @param string $end_time
	 * @return bool
	 */
	public static function availableTime($day_id, $room_id, $start_time, $end_time)
	{
		$allocations = Allocation::where([['day_id', $day_id], ['room_id', $room_id], ['ends_at', '>', $start_time], ['starts_at', '<', $end_time]])->count();

		if ($allocations > 0)
		{
			return false;
		}
		return true;
	}

	/**
	 * Checks to see if the specified staff member is already teaching 
	 * in another location between the specified start and end
	 * times. If they are, a falsey value is returned.
	 *
	 * @param int $day_id
	 * @param int $user_id
	 * @param string $start_time
	 * @param string $end_time
	 * @return bool
	 */
	public static function omnipresence($day_id, $user_id, $start_time, $end_time)
	{
		$allocations = Allocation::where([['day_id', $day_id], ['staff_user_id', $user_id], ['ends_at', '>', $start_time], ['starts_at', '<', $end_time]])->count();

		if ($allocations > 0) return false;
		
		return true;
	}


	/**
	 * Checks to see if the selected staff member will have been teaching
	 * for more than 6 hours on the chosen day. Returns falsey if
	 * the staff member has/will exceed the limit.
	 *
	 * @param int $user_id
	 * @param int $day_id
	 * @return bool
	 */
	public static function consecutiveTeachingHours($user_id, $day_id)
	{
		$query = DB::table('allocations')
		  ->select(DB::raw('SUM(TIMESTAMPDIFF(HOUR, starts_at, ends_at)) AS totalHours'))
		  ->where(['day_id' => $day_id, 'staff_user_id' => $user_id])
		  ->first();

		return (($query->totalHours >= 6) ? false : true);
	}

}