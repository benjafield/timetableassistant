<?php

namespace Cbenjafield\Albert;

use Illuminate\Support\ServiceProvider;

class AlbertServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBertie();

        $this->app->alias('bertie', 'Cbenjafield\Albert\Bertie');
    }

    protected function registerBertie()
    {

        $this->app->singleton('bertie', function ($app) {
            return new Bertie;
        });

    }

    public function provides()
    {
        return ['bertie', 'Cbenjafield\Albert\Bertie'];
    }
}
