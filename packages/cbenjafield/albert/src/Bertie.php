<?php

namespace Cbenjafield\Albert;

use Carbon\Carbon as Carbon;
use BadMethodCallException;
use Illuminate\Support\Traits\Macroable;
use Cbenjafield\Albert\Timetabler;

class Bertie
{
	use Macroable {
		Macroable::__call as macroCall;
	}

	use Timetabler;

	public function relativeGreeting($capitalise = TRUE, $withName = FALSE)
	{
		$greeting = 'evening';

		if (Carbon::now()->hour < 12) $greeting = 'morning';
		if (Carbon::now()->hour >= 12 && Carbon::now()->hour < 18) $greeting = 'afternoon';

		return (($capitalise) ? 'Good ' . $greeting : 'good ' . $greeting);
	}

	//---
	public function __call($method, $params)
	{
		try {
			return $this->componentCall($method, $params);
		} catch (BadMethodCallException $e) {
			//
		}

		try {
			return $this->macroCall($method, $params);
		} catch (BadMethodCallException $e) {
			//
		}

		throw new BadMethodCallException("Method {$method} does not exist.");
	}

}