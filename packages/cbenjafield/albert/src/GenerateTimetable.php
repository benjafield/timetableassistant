<?php

namespace Cbenjafield\Albert;

use Cbenjafield\Albert\TimetableHtml;

use App\Room;
use App\Allocation;

use Illuminate\Support\HtmlString;

trait GenerateTimetable
{

	use TimetableHtml;

	protected $times = [
		'09:00', '10:00', '11:00', '12:00', '13:00', '14:00', '15:00', '16:00', '17:00'
	];

	/**
	 * Create an Allocation.
	 *
	 * @param array $data
	 * @return int
	 */
	public static function createAllocation($data)
	{
		/**
		 * ------------------------------------------
		 * Here the constraint rules will be run
		 * against the allocation data. Will
		 * tackle this at later date.
		 * ------------------------------------------
		 */

		/**
		 * For now, just create the allocation
		 */
		$data['user_id'] = auth()->user()->id;
		$allocation = new Allocation($data);
		$allocation->save();

		/**
		 * Return the ID of the newly created allocation.
		 */
		return $allocation->id;
	}

	/**
	 * Draw the timetable for a room.
	 *
	 * @param int $id
	 * @return \Illuminate\Support\HtmlString
	 */
	public function drawRoomTimetable($id, $allocations)
	{
		/**
		 * Get the room.
		 */
		$room = Room::findOrFail($id);

		return $this->timetableHtml($this->times, $allocations);

	}

	/**
	 * Draw a daily room timetable.
	 */
	public function drawRoomTimetableByDay($day, $semester_id = '')
	{
		$allocations = Room::allocationsByDay($day, $semester_id);

		$days = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday'
        ];

		return $this->timetableHtml($this->times, $allocations, $days[$day]);
	}

	//---
	public function __call($method, $params)
	{
		try {
			return $this->componentCall($method, $params);
		} catch (BadMethodCallException $e) {
			//
		}

		try {
			return $this->macroCall($method, $params);
		} catch (BadMethodCallException $e) {
			//
		}

		throw new BadMethodCallException("Method {$method} does not exist.");
	}

}