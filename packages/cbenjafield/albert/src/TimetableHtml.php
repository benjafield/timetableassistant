<?php

namespace Cbenjafield\Albert;
use Carbon\Carbon as Carbon;

trait TimetableHtml
{

	/**
	 * Generates the Table HTML
	 */
	public function timetableHtml($x = [], $y = [], $title = '')
	{

		$html = '<table class="Timetable__table">' .
			    $this->theadHtml($x, $title) .
			    $this->tbodyHtml($y) .
			    '</table>';

		return $html;

	}

	/**
	 * Generates the Table Head HTML
	 *
	 * @param array $headings
	 * @return string
	 */
	public function theadHtml($headings = [], $title = '')
	{
		$cols = $this->htmlChildren('th', $headings);

		return '<thead><tr><th class="Title">'.$title.'</th>'.$cols.'</tr></thead>';
	}

	/**
	 * Generates the Table Body Html
	 */
	public function tbodyHtml($rows = [])
	{
		$html = '';

		foreach($rows as $heading => $cols)
		{
			$html .= '<tr><th>'.$heading.'</th>';

			foreach($cols as $col)
			{
				if (empty($col))
				{
					$html .= '<td></td>';
				}
				else
				{
					$html .= '<td>';
					foreach($col as $allocation)
					{
						$html .= $this->allocationHtml($allocation);
					}
					$html .= '</td>';
				}
			}

			$html .= '</tr>';
		}

		return '<tbody>'.$html.'</tbody>';
	}

	/**
	 * Generate Child Elements
	 *
	 * @param string $el
	 * @param array $children
	 * @return string
	 */
	public function htmlChildren($el, $children = [])
	{
		$html = '';

		foreach($children as $content)
		{
			$html .= '<'.$el.'>' . $content . '</'.$el.'>';
		}

		return $html;
	}

	/**
	 * Generate Allocation HTML
	 */
	public function allocationHtml($allocation)
	{

		$starts = Carbon::parse($allocation['starts_at']);
		$ends = Carbon::parse($allocation['ends_at']);

		$diff = $starts->diffInHours($ends);

		$html = '<span class="Allocation__module">' . $allocation['module']['name'] . '</span>';

		return '<div class="Allocation" style="background-color:'.$allocation['module']['colour'].';width:'.($diff*100).'%;" data-length="'.$diff.'"' . ((auth()->check()) ? ' onclick="window.location.href=\''.url('allocations/' . $allocation['id'] . '/edit').'\'"' : '') . '>' . $html . '</div>';

	}

}