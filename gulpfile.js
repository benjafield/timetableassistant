var elixir = require('laravel-elixir');

/*
 | Require Jeffrey Way's Elixir Stylus add-on.
 */
require('laravel-elixir-stylus');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

	/*
	 | File to compile/mix.
	 */
    mix.stylus(['linear.styl', 'app.styl']);

    /*
     | Mix Scripts
     */
    mix.scripts(['app.js']);
});
