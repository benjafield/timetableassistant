<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{

	/**
     * Use the Soft Deletes Laravel functionality
     */
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'courses';

    protected $fillable = ['name', 'colour', 'leader_user_id', 'department_id', 'user_id'];
}
