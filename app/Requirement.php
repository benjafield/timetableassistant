<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    
	/**
	 * Declare the mass assignable fields
	 */
	protected $fillable = ['name'];

	/**
	 * Turn timestamps off
	 */
	public $timestamps = FALSE;

	/**
	 * Declare the relationship to rooms
	 */
	public function rooms()
	{
		return $this->belongsToMany('App\Room');
	}

	/**
	 * Declare the relationship to allocations
	 */
	public function allocations()
	{
		return $this->belongsToMany('App\Allocation');
	}

	/**
	 * Generate Key Value pairings for laravel collective.
	 */
	public static function formSelect()
	{
		$requirements = Requirement::get();
		$reqs = [];

		foreach($requirements as $req)
		{
			$reqs[$req->id] = $req->name;
		}

		return $reqs;
	}

}
