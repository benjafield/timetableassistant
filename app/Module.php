<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
    /**
     * Use Laravel's Soft Delete functionality
     */
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $table = 'modules';

    protected $fillable = ['code', 'name', 'colour', 'leader_user_id', 'user_id'];

    /**
     * Build module form select array.
     *
     * @return array
     */
    public static function formSelect()
    {
    	$modules = self::get(['id', 'name', 'code']);
    	$modArray = [];

    	foreach($modules as $module)
    	{
    		$modArray[$module->id] = $module->code . ' - ' .$module->name;
    	}

    	return $modArray;
    }

    /**
     * A module can have one or many allocations
     */
    public function allocations()
    {
        return $this->hasMany('App\Allocation');
    }

    /**
     * Declare the relationship to requirements
     */
    public function requirements()
    {
        return $this->belongsToMany('App\Requirement', 'module_requirement');
    }

}
