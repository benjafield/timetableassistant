<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Semester extends Model
{
    protected $table = 'semesters';

    protected $fillable = ['label', 'start_date', 'end_date'];

    protected $dates = ['start_date', 'end_date'];

    /**
     * Every Semester can have many allocations.
     */
    public function allocations()
    {
    	return $this->hasMany('App\Allocation');
    }

    /**
     * Get the current semester
     */
    public static function current()
    {
        $now = Carbon::now()->format('Y-m-d');
        $semester = self::where('start_date', '<=', $now)->where('end_date', '>=', $now)->first();

        if(count($semester) < 1) $semester = Semester::orderBy('created_at', 'desc')->first();

        return $semester;
    }

    /**
     * Build semester form select array.
     *
     * @return array
     */
    public static function formSelect()
    {
        $semesters = self::get(['id', 'label']);
        $semArray = [
            'data' => [],
            'current' => self::current()->id
        ];

        foreach($semesters as $semester)
        {
            $semArray['data'][$semester->id] = $semester->label;
        }

        return $semArray;
    }
}
