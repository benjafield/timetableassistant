<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
	/**
	 * Use Laravel's Soft Delete functionality 
	 */
	use SoftDeletes;

	protected $dates = ['deleted_at'];

	protected $table = 'departments';

	protected $fillable = ['name', 'leader_user_id', 'user_id'];

	/**
	 * Define the reverse to the one to many relationship with the
	 * user model.
	 */
	public function user()
	{
		return $this->belongsTo('App\User');
	}
}
