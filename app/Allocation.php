<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Allocation extends Model
{
    /**
     * Use Laravel's Soft Delete functionality
     */
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['module_id', 'room_id', 'semester_id', 'staff_user_id', 'day_id', 'starts_at', 'ends_at', 'user_id', 'notes'];

    /**
     * Every allocation belongs to a semester.
     */
    public function semester()
    {
    	return $this->belongsTo('App\Semester');
    }

    /**
     * Every Allocation has a module
     */
    public function module()
    {
        return $this->belongsTo('App\Module');
    }

    /**
     * Get Daily Room Allocations
     */
    public static function dailyRoomAllocations($semesterId, $roomId)
    {

        $times = [
            '09:00:00' => [],
            '10:00:00' => [],
            '11:00:00' => [],
            '12:00:00' => [],
            '13:00:00' => [],
            '14:00:00' => [],
            '15:00:00' => [],
            '16:00:00' => [],
            '17:00:00' => []
        ];

        $days = [
            \Carbon\Carbon::MONDAY     => 'Monday',
            \Carbon\Carbon::TUESDAY    => 'Tuesday',
            \Carbon\Carbon::WEDNESDAY  => 'Wednesday',
            \Carbon\Carbon::THURSDAY   => 'Thursday',
            \Carbon\Carbon::FRIDAY     => 'Friday'
        ];

        $dailyAllocations = [
            \Carbon\Carbon::MONDAY    => $times,
            \Carbon\Carbon::TUESDAY   => $times,
            \Carbon\Carbon::WEDNESDAY => $times,
            \Carbon\Carbon::THURSDAY  => $times,
            \Carbon\Carbon::FRIDAY    => $times
        ];

        $allocations = self::where(['semester_id' => $semesterId, 'room_id' => $roomId])->with('module')->get();
        $grouped = $allocations->groupBy('day_id');
        $grouped = $grouped->toArray();

        foreach($grouped as $key => $data)
        {
            foreach($data as $alc)
            {
                $dailyAllocations[$alc['day_id']][$alc['starts_at']][] = $alc;
            }
        }

        return array_combine(array_values($days), array_values($dailyAllocations));

    }

    /**
     * Declare the relationship to requirements
     */
    public function requirements()
    {
        return $this->belongsToMany('App\Requirement');
    }

}
