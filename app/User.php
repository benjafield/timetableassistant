<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'email', 'password', 'status_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The relationship between a user and departments
     */
    public function departments()
    {
        return $this->hasMany('App\Department');
    }

    /**
     * Form the relationship between a user and courses
     */
    public function courses()
    {
        return $this->hasMany('App\Course');
    }

    /**
     * Form the relationship between a user and modules
     */
    public function modules()
    {
        return $this->hasMany('App\Module');
    }

    /**
     * Define the relationship between a user and rooms
     */
    public function rooms()
    {
        return $this->hasMany('App\Room');
    }

    /**
     * Build module form select array.
     *
     * @return array
     */
    public static function formSelect()
    {
        $users = self::get(['id', 'firstname', 'lastname']);
        $userArray = [];

        foreach($users as $user)
        {
            $userArray[$user->id] = $user->firstname . ' ' . $user->lastname;
        }

        return $userArray;
    }
}
