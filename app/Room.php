<?php

namespace App;

use App\Semester;
use App\Allocation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{

	/**
	 * Use Laravel's Soft Delete functionality
	 */
	use SoftDeletes;

	protected $dates = ['deleted_at'];
    
	protected $table = 'rooms';

	protected $fillable = [
		'label', 'max_students'
	];

	/**
	 * Get room allocations by day
	 */
	public static function allocationsByDay($day_id, $semester_id = '')
	{
		$semester_id = ((!empty($semester_id)) ? $semester_id : Semester::current()->id);

		$times = [
        	'09:00:00' => [], '10:00:00' => [], '11:00:00' => [], '12:00:00' => [], '13:00:00' => [], '14:00:00' => [], '15:00:00' => [], '16:00:00' => [], '17:00:00' => []
        ];

		$rooms = self::get();
		$roomNames = [];
		$roomAllos = [];
		$allocations = Allocation::where(['semester_id' => $semester_id, 'day_id' => $day_id])->with('module')->get();
		$grouped = $allocations->groupBy('room_id');
		$grouped = $grouped->toArray();

		foreach($rooms as $room)
		{
			$roomNames[$room->id] = $room->label;
			$roomAllos[$room->id] = $times;
		}

		foreach($grouped as $key => $data)
		{
			foreach($data as $alc)
			{
				$roomAllos[$alc['room_id']][$alc['starts_at']][] = $alc;
			}
		}

		return array_combine(array_values($roomNames), array_values($roomAllos));

	}

	/**
	 * Declare the relationship to requirements
	 */
	public function requirements()
	{
		return $this->belongsToMany('App\Requirement', 'room_requirement');
	}

    /*
	 * Return the rooms as a form select.
	 */
	public static function formSelect()
	{
		$rooms = Room::get();
		$rooms_arr = [];

		foreach($rooms as $room)
		{
			$rooms_arr[$room->id] = $room->label;
		}

		return $rooms_arr;
	}

}
