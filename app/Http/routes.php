<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    
	Route::get('/', 'HomeController@index');
	Route::get('login', 'Auth\AuthController@getLogin');
	Route::post('login', 'Auth\AuthController@postLogin');
	Route::get('register', 'Auth\AuthController@getRegister');
	Route::post('register', 'Auth\AuthController@postRegister');
	Route::get('logout', 'Auth\AuthController@logout');

	/**
	 * Route for external behaviours and functionality
	 */
	Route::get('search', 'HomeController@rooms');
	Route::get('timetable/{label}', 'TimetableController@room');

});

Route::group(['middleware' => ['web', 'auth']], function () {

	/**
	 | Timetabling Routes
	 |
	 | Involved in creating allocations (sessions) and
	 | assigning them to rooms.
	 */
	Route::get('rooms/{rooms}/sessions/create', 'RoomsController@createAllocation');
	Route::post('rooms/{rooms}/sessions', 'RoomsController@storeAllocation')->name('allocations.store');
	
	Route::post('rooms/{id}/requirements', 'RoomsController@attachRequirement');
	Route::delete('rooms/{id}/requirements/{requirement_id}', 'RoomsController@detachRequirement');

	/**
	 * Trash
	 */
	Route::get('departments/trash', 'DepartmentsController@trash');
	Route::get('rooms/trash', 'RoomsController@trash');
	Route::get('courses/trash', 'CoursesController@trash');
	Route::get('modules/trash', 'ModulesController@trash');

	Route::get('departments/{id}/restore', 'DepartmentsController@restore');
	Route::get('rooms/{id}/restore', 'RoomsController@restore');
	Route::get('courses/{id}/restore', 'CoursesController@restore');
	Route::get('modules/{id}/restore', 'ModulesController@restore');

	/**
	 * Rooms
	 */
	Route::get('rooms/import', 'RoomsController@import');
	Route::resource('rooms', 'RoomsController');

	/**
	 * Courses
	 */
	Route::get('rooms/import', 'CoursesController@import');
	Route::resource('courses', 'CoursesController');

	/**
	 * Departments
	 */
	Route::get('departments/import', 'DepartmentsController@import');
	Route::resource('departments', 'DepartmentsController');

	/**
	 * Modules
	 */
	Route::get('modules/import', 'ModulesController@import');
	Route::resource('modules', 'ModulesController');

	Route::post('modules/{id}/requirements', 'ModulesController@attachRequirement');
	Route::delete('modules/{id}/requirements/{requirement_id}', 'ModulesController@detachRequirement');

	/**
	 * Semesters
	 */
	Route::get('semesters/{semester_id}/allocations/create', 'SemestersController@createAllocation');
	Route::post('semesters/{semester_id}/allocations', 'SemestersController@storeAllocation');
	Route::get('semesters/{semester_id}/allocations/{allocation_id}/edit', 'SemestersController@editAllocation');
	Route::put('semesters/{semester_id}/allocations/{allocation_id}', 'SemestersController@updateAllocation');
	Route::resource('semesters', 'SemestersController');

	/**
	 * Account
	 */
	Route::get('account/settings', 'AccountController@settings');
	Route::put('account/settings', 'AccountController@postSettings');

	/**
	 * Timetable Generation
	 */
	Route::get('timetables/generate', 'TimetableController@generator');

	/**
	 * Staff
	 */
	Route::resource('staff', 'StaffController');

	/**
	 * Allocations
	 */
	Route::get('allocations/{id}/edit', 'AllocationsController@edit');
	Route::put('allocations/{id}', 'AllocationsController@update');

	/**
	 * Requirements
	 */
	Route::resource('requirements', 'RequirementsController');

});
