<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;

class AllocationsSemesterRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) return true;

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'module_id'     => 'required|numeric',
            'semester_id'   => 'required|numeric',
            'staff_user_id' => 'required|numeric|staff_bounds',
            'day_id'        => 'required|numeric',
            'starts_at'     => 'required|date_format:H:i:s|available_semester_time',
            'ends_at'       => 'required|date_format:H:i:s|after:starts_at'
        ];
    }
}
