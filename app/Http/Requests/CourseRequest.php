<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class CourseRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (Auth::check()) return true;

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->route()->getName() == 'courses.create') 
        {
            return [
                'name' => 'required|unique:courses',
                'department_id' => 'required'
            ];
        }

        return [
            'name' => 'required|unique:courses,name,' . $this->route()->getParameter('courses'),
            'department_id' => 'required'
        ];
    }
}
