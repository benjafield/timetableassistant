<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Room;
use App\Module;
use App\Semester;
use App\Allocation;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Cbenjafield\Albert\Bertie as Bertie;

class RoomsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::get();

        return view('app.rooms.all', compact('rooms'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.rooms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\RoomRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\RoomRequest $request)
    {
        $room = new Room($request->all());
        $userRoom = auth()->user()->rooms()->save($room);

        return redirect('rooms/' . $userRoom->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   
        $room = Room::findOrFail($id);
        $semester = Semester::current();

        $allocations = Allocation::dailyRoomAllocations($semester->id, $id);

        return view('app.rooms.show', compact('room', 'semester', 'allocations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $room = Room::with('requirements')->findOrFail($id);

        return view('app.rooms.edit', compact('room'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\RoomRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\RoomRequest $request, $id)
    {

        $room = Room::findOrFail($id);
        $room->update($request->all());

        return redirect('rooms/' . $room->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $room = Room::withTrashed()->findOrFail($id);

        if ($request->has('permanent'))
        {
            $room->forceDelete();
        }
        else
        {
            $room->delete();
        }

        return redirect()->back();
    }

    /**
     * View all courses in trash
     *
     * @return \Illuminate\Http\Response
     */
    public function trash()
    {
        $rooms = Room::onlyTrashed()->get();
        $trash = TRUE;

        return view('app.rooms.all', compact('rooms', 'trash'));
    }

    /**
     * Restore a soft deleted resource
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $room = Room::withTrashed()->where('id', $id)->restore();
        return redirect()->back();
    }

    /**
     | Timetabling Functionality
     | ---------------------------------
     */

    /**
     * Show the form to add an allocation to a room.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function createAllocation($id)
    {
        $room = Room::findOrFail($id);

        return view('app.allocations.create', compact('room'));
    }

    /**
     * Store the allocation in storage.
     *
     * @param \App\Http\Requests\AllocationRequest $request
     * @param int $id
     * @param \Illuminate\Http\Response
     */
    public function storeAllocation(Requests\AllocationRequest $request, $id)
    {
        $data = [
            'room_id'       => $id,
            'module_id'     => $request->input('module_id'),
            'semester_id'   => $request->input('semester_id'),
            'staff_user_id' => $request->input('staff_user_id'),
            'day_id'        => $request->input('day_id'),
            'starts_at'     => $request->input('starts_at'),
            'ends_at'       => $request->input('ends_at'),
            'notes'         => $request->input('notes')
        ];

        $allocation = Bertie::createAllocation($data);

        return redirect('rooms/' . $id);
    }

    /**
     * Post request to add a requirement to the specified
     * room by ID. Create the many-to-many attachment.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function attachRequirement(Request $request, $id)
    {
        $this->validate($request, [
            'requirement_id' => 'required|numeric'
        ]);

        $room = Room::findOrFail($id);

        $room->requirements()->attach($request->input('requirement_id'));

        return back();
    }

    /**
     * Detach a requirement from a room.
     *
     * @param int $id
     * @param int $requirement_id
     * @return \Illuminate\Http\Response
     */
    public function detachRequirement($id, $requirement_id)
    {
        $room = Room::findOrFail($id);
        $room->requirements()->detach($requirement_id);

        return back();
    }
}
