<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    
	public function settings()
	{
		$user = auth()->user();

		return view('app.account.settings', compact('user'));
	}

	public function postSettings(Request $request)
	{

		$valData = [
			'firstname' => 'required',
			'lastname'  => 'required',
			'email'     => 'required|email|unique:users,email,' . auth()->user()->id,
			'password'  => 'sometimes|confirmed'
		];

		$this->validate($request, $valData);

		$data = [
			'firstname' => $request->input('firstname'),
			'lastname'  => $request->input('lastname'),
			'email'     => $request->input('email')
		];

		if (!empty($request->input('password')))
		{
			$data['password'] = bcrypt($request->input('password'));
		}

		$user = User::findOrFail(auth()->user()->id)
		            ->update($data);

		return back();
	}

}
