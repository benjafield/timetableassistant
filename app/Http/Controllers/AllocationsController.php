<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Allocation;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AllocationsController extends Controller
{
    
    /**
     * Show the form to edit an Allocation
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
	public function edit($id)
	{
		/**
		 * Find the allocation in storage or display a 404 page.
		 */
		$allocation = Allocation::findOrFail($id);

		/**
		 * Return the edit view.
		 */
		return view('app.allocations.edit', compact('allocation'));
	}

	/**
	 * Update the allocation in storage.
	 *
	 * @param \App\Http\Requests\AllocationRequest $request 
	 * @param int $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Requests\AllocationRequest $request, $id)
	{
		$allocation = Allocation::findOrFail($id);

		$allocation->update($request->all());

		/**
		 * Return the the referring page if the
		 * update was successful.
		 */
		return back();
	}

}
