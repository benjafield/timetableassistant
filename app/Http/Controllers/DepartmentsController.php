<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Department;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::get();

        return view('app.departments.all', compact('departments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\DepartmentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\DepartmentRequest $request)
    {
        $department = new Department($request->all());
        $userDepartment = auth()->user()->departments()->save($department);

        return redirect('departments/' . $userDepartment->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $department = Department::findOrFail($id);

        return view('app.departments.show', compact('department'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $department = Department::findOrFail($id);

        return view('app.departments.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\DepartmentRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\DepartmentRequest $request, $id)
    {
        $department = Department::findOrFail($id);
        $department->update($request->all());

        return redirect('departments/' . $department->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $department = Department::withTrashed()->findOrFail($id);

        if ($request->has('permanent'))
        {
            $department->forceDelete();
        }
        else
        {
            $department->delete();
        }

        return redirect()->back();
    }

    /**
     * View all courses in trash
     *
     * @return \Illuminate\Http\Response
     */
    public function trash()
    {
        $departments = Department::onlyTrashed()->get();
        $trash = TRUE;

        return view('app.departments.all', compact('departments', 'trash'));
    }

    /**
     * Restore a soft deleted resource
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $department = Department::withTrashed()->where('id', $id)->restore();
        return redirect()->back();
    }
}
