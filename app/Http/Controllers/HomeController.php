<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Room;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    
	/**
	 * Index method will serve as the default page for the
	 * application. If the user is logged in they will
	 * see the dashboard, otherwise the login page.
	 */
	public function index()
	{

		/**
		 * If the user is logged in run the _user method.
		 */
		if (Auth::check()) return $this->_user();

		/**
		 * Run the _guest() method if the user is not logged in.
		 */
		return $this->_guest();
	}

	/**
	 * _guest private method is run when the user is not logged
	 * in and is viewing the index method of this controller.
	 */
	private function _guest()
	{
		return view('auth.login');
	}

	/**
	 * _user private method is run when the user is logged in
	 * and is viewing the index method of this controller.
	 * Shows the dashboard of the application.
	 */
	private function _user()
	{
		return view('app.dashboard');
	}

	/**
	 * Room function returns all rooms search results page based
	 * on the provided search term. If a search term has
	 * not been provided, an input is shown.
	 */
	public function rooms(Request $request)
	{

		$term = (($request->has('q')) ? $request->get('q') : '');

		$title = ((empty($term)) ? 'Search for room' : 'Search results for "'.$term.'"');

		$results = ((!empty($term)) ? Room::where('label', 'LIKE', '%'.$term.'%')->get() : []);

		return view('external.search', compact('term', 'results', 'title'));

	}

}
