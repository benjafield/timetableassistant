<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Course;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class CoursesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::get();
        return view('app.courses.all', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\CourseRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CourseRequest $request)
    {
        $course = new Course($request->all());    
        $userCourse = auth()->user()->courses()->save($course);

        return redirect('courses/' . $userCourse->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $course = Course::findOrFail($id);

        return view('app.courses.show', compact('course'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::findOrFail($id);

        return view('app.courses.edit', compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $course = Course::findOrFail($id);
        $course->update($request->all());

        return redirect('courses/' . $course->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $course = Course::withTrashed()->findOrFail($id);

        if ($request->has('permanent'))
        {
            $course->forceDelete();
        }
        else
        {
            $course->delete();
        }

        return redirect()->back();
    }

    /**
     * View all courses in trash
     *
     * @return \Illuminate\Http\Response
     */
    public function trash()
    {
        $courses = Course::onlyTrashed()->get();
        $trash = TRUE;

        return view('app.courses.all', compact('courses', 'trash'));
    }

    /**
     * Restore a soft deleted resource
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $course = Course::withTrashed()->where('id', $id)->restore();
        return redirect()->back();
    }
}
