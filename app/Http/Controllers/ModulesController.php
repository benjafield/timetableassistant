<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Module;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::get();
        return view('app.modules.all', compact('modules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.modules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\ModuleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\ModuleRequest $request)
    {
        $module = new Module($request->all());    
        $userModule = auth()->user()->Modules()->save($module);

        return redirect('modules/' . $userModule->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module = Module::findOrFail($id);

        return view('app.modules.show', compact('module'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $module = Module::with('requirements')->findOrFail($id);

        return view('app.modules.edit', compact('module'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $module = Module::findOrFail($id);
        $module->update($request->all());

        return redirect('modules/' . $module->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $module = Module::withTrashed()->findOrFail($id);

        if ($request->has('permanent'))
        {
            $module->forceDelete();
        }
        else
        {
            $module->delete();
        }

        return redirect()->back();
    }

    /**
     * View all Modules in trash
     *
     * @return \Illuminate\Http\Response
     */
    public function trash()
    {
        $modules = Module::onlyTrashed()->get();
        $trash = TRUE;

        return view('app.modules.all', compact('modules', 'trash'));
    }

    /**
     * Restore a soft deleted resource
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function restore($id)
    {
        $module = Module::withTrashed()->where('id', $id)->restore();
        return redirect()->back();
    }

    /**
     * Post request to add a requirement to the specified
     * room by ID. Create the many-to-many attachment.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function attachRequirement(Request $request, $id)
    {
        $this->validate($request, [
            'requirement_id' => 'required|numeric'
        ]);

        $module = Module::findOrFail($id);

        $module->requirements()->attach($request->input('requirement_id'));

        return back();
    }

    /**
     * Detach a requirement from a room.
     *
     * @param int $id
     * @param int $requirement_id
     * @return \Illuminate\Http\Response
     */
    public function detachRequirement($id, $requirement_id)
    {
        $module = Module::findOrFail($id);
        $module->requirements()->detach($requirement_id);

        return back();
    }
}
