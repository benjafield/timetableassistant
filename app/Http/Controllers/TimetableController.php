<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Room;
use App\Allocation;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TimetableController extends Controller
{
    
    /**
     * Display a timetable for the specified room.
     * The label of the room is supplied.
     *
     * @param string $label
     * @return \Illuminate\Http\Response
     */
	public function room($label)
	{
		$room = Room::where('label', $label)->firstOrFail();
		$allocations = Allocation::dailyRoomAllocations(3, $room->id);

		return view('external.timetable', compact('room', 'allocations'));
	}

	/**
	 * Display the page to set up timetable generation.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function generator()
	{
		return view('app.timetables.generator');
	}

}
