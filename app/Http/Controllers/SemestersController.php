<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Room;
use App\Semester;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class SemestersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $semesters = Semester::get();

        return view('app.semesters.all', compact('semesters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.semesters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SemesterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\SemesterRequest $request)
    {
        $semester = new Semester($request->all());
        $semester->save();

        return redirect('semesters');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $semester = Semester::findOrFail($id);
        $rooms = Room::get();

        $days = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday'
        ];

        return view('app.semesters.show', compact('semester', 'rooms', 'days'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $semester = Semester::findOrFail($id);

        return view('app.semesters.edit', compact('semester'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SemesterRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\SemesterRequest $request, $id)
    {
        $semester = Semester::findOrFail($id);
        $semester->update($request->all());

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $semester = Semester::findOrFail($id);
        $semester->delete();

        return redirect('semesters');
    }

    /**
     * Show the form to create a new allocation on a semester.
     *
     * @param int $semester_id
     * @return \Illuminate\Http\Response
     */
    public function createAllocation($semester_id)
    {
        $semester = Semester::findOrFail($semester_id);
        return view('app.allocations.create', compact('semester'));
    }

    /**
     * Store the new semester allocation in application storage.
     *
     * @param \App\Http\Requests\AllocationRequest $request
     * @param int $semester_id
     * @return \Illuminate\Http\Response
     */
    public function storeAllocation(Requests\AllocationRequest $request, $semester_id)
    {
        $semester = Semester::findOrFail($semester_id);
        
        $data = [
            'room_id'       => $request->input('room_id'),
            'module_id'     => $request->input('module_id'),
            'semester_id'   => $semester->id,
            'staff_user_id' => $request->input('staff_user_id'),
            'day_id'        => $request->input('day_id'),
            'starts_at'     => $request->input('starts_at'),
            'ends_at'       => $request->input('ends_at'),
            'notes'         => $request->input('notes')
        ];

        $allocation = Bertie::createAllocation($data);

        /**
         * Return back to the Semester page.
         */
        return redirect('semesters/' . $semester_id);
    }

    /**
     * Show the form to edit an allocation from within a Semester.
     *
     * @param int $semester_id
     * @param int $allocation_id
     * @return \Illuminate\Http\Response
     */
    public function editAllocation($semester_id, $allocation_id)
    {
        $semester = Semester::findOrFail($semester_id);
        $allocation = $semester->allocations()->findOrFail($allocation_id);
        return view('app.allocations.edit', compact('semester', 'allocation'));
    }

    /**
     * Update the allocation in application storage.
     *
     * @param \App\Http\Requests\AllocationRequest $request
     * @param int $semester_id
     * @param int $allocation_id
     * @return \Illuminate\Http\Response
     */
    public function updateAllocation(Requests\AllocationRequest $request, $semester_id, $allocation_id)
    {
        $semester = Semester::findOrFail($semester_id);
        $allocation = $semester->allocations()->findOrFail($allocation_id);

        $allocation->update($request->all());

        /**
         * Return back to the edit allocation form.
         */
        return back();
    }
}
