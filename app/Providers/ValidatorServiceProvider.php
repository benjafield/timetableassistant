<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Request;

use Cbenjafield\Albert\Bertie;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Confirms that the time is available.
         */
        Validator::extendImplicit('available_time', function ($attribute, $value, $parameters) {
            if (empty(Request::segment(2))) return false;
            if (!Request::has('day_id') || !Request::has('ends_at')) return false;
            return Bertie::availableTime(Request::input('day_id'), Request::segment(2), $value, Request::input('ends_at'));
        });

        /**
         * Confirms that the staff member is not in more than one
         * place at the same time.
         */
        Validator::extendImplicit('omnipresence', function ($attribute, $value, $parameters) {
            if (!Request::has('day_id') || !Request::has('ends_at') || !Request::has('starts_at')) return false;
            return Bertie::omnipresence(Request::input('day_id'), $value, Request::input('starts_at'), Request::input('ends_at'));
        });

        /**
         * Confirms that the staff member has not been teaching for longer than 6 hours in
         * a day.
         */
        Validator::extendImplicit('staff_bounds', function ($attribute, $value, $parameters) {
            if (!Request::has('staff_user_id') || !Request::has('day_id')) return false;
            return Bertie::consecutiveTeachingHours(Request::input('staff_user_id'), Request::input('day_id'));
        });

        /**
         * Validation for the available semester time
         */
        Validator::extendImplicit('available_semester_time', function ($attribute, $value, $parameters) {
            if (!Request::has('day_id') || !Request::has('ends_at') || !Request::has('room_id')) return false;
            return Bertie::availableTime(Request::input('day_id'), Request::input('room_id'), $value, Request::input('ends_at'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
