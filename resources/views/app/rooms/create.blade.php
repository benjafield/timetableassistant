@extends('layouts.app')
@section('title', 'New Room')

@section('content')

<header class="Header__content">
	<h1>New Room</h1>
	<div class="Tools">
		<a href="{{ url('rooms/import') }}"><i class="icon-upload2"></i> Import</a>
	</div>
</header>

{!! Form::open(['url' => 'rooms', 'id' => 'roomForm']) !!}

@include('partials.forms.room', ['buttonText' => 'Create'])

{!! Form::close() !!}

@stop