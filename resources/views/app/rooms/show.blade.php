@extends('layouts.app')
@section('title', 'Room: ' . $room->label)

@section('content')

<header class="Header__content">
	<h1>Room: {{ $room->label }} <small>Max students: {{ $room->max_students }}</small></h1>
	<div class="Tools">
		<a href="{{ url('rooms/' . $room->id . '/edit') }}"><i class="icon-pencil"></i> Edit</a>
		<a href="{{ url('rooms/' . $room->id . '/sessions/create') }}"><i class="icon-alarm-add2"></i> New Session</a>
	</div>
</header>

<p class="notice">You are currenty viewing sessions for <strong>{{ $semester->label }}</strong>. <a class="Button --small">Change <i class="icon-chevron-down"></i></a></p>

<div class="Timetable --extra-top">
	{!! Bertie::drawRoomTimetable($room->id, $allocations) !!}
</div>

@stop