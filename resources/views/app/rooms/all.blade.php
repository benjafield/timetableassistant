@extends('layouts.app')
@section('title', ((isset($trash) && $trash) ? 'Deleted Rooms' : 'Rooms'))

@section('content')

<header class="Header__content">
	<h1>{!! ((isset($trash) && $trash) ? '<a href="'. url('rooms') .'"><i class="icon-arrow-left rounded"></i></a> Deleted ' : '') !!}Rooms</h1>
	<div class="Tools">
		<a href="{{ url('rooms/create') }}"><i class="icon-plus"></i> New</a>
		<a href="{{ url('rooms/import') }}"><i class="icon-upload2"></i> Import</a>
		@if (!isset($trash) || !$trash)
		<a href="{{ url('rooms/trash') }}"><i class="icon-trash"></i> Deleted</a>
		@endif
	</div>
</header>

<table>
	<thead>
		<tr>
			<th width="50">#</th>
			<th>Room</th>
			<th width="200">Created</th>
			<th width="100"><i class="icon-cog"></i></th>
		</tr>
	</thead>
	<tbody>
	@foreach($rooms as $room)
		<tr>
			<td>{{ $room->id }}</td>
			<td><a href="{{ url('rooms/' . $room->id) }}">{{ $room->label }}</a></td>
			<td>{{ $room->created_at->diffForHumans() }}</td>
			<td>
				@include('partials.forms.delete', ['id' => $room->id, 'resource' => 'rooms', 'trash' => ((isset($trash) && $trash) ? TRUE : FALSE)])
			</td>
		</tr>
	@endforeach
	</tbody>
</table>

@stop