@extends('layouts.app')
@section('title', 'Edit Room: ' . $room->label)

@section('content')

<header class="Header__content">
	<h1>Edit Room: {{ $room->label }}</h1>
</header>

{!! Form::model($room, ['url' => 'rooms/' . $room->id, 'method' => 'PUT', 'id' => 'roomForm']) !!}

@include('partials.forms.room', ['buttonText' => 'Edit'])

{!! Form::close() !!}

<hr>

<h2>Requirements</h2>

<div class="Row">
	<table>
		<thead>
			<tr>
				<th>Name</th>
				<th><i class="icon-cog"></i></th>
			</tr>
		</thead>
		<tbody>
		@foreach($room->requirements as $requirement)
			<tr>
				<td>{{ $requirement->name }}</td>
				<td>
					{!! Form::open(['url' => 'rooms/' . $room->id . '/requirements/' . $requirement->id, 'method' => 'DELETE']) !!}
					<button class="Button --small --blend" type="submit"><i class="icon-trash"></i></button>
					{!! Form::close() !!}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>

<div class="Row">
	{!! Form::open(['url' => 'rooms/' . $room->id . '/requirements']) !!}
	<div class="inputs">
		{!! Form::label('requirement_id', 'Apply a requirement') !!}
		{!! Form::select('requirement_id', \App\Requirement::formSelect(), null, ['placeholder' => 'Please Select...']) !!}
	</div>
	<div class="inputs">
		<button type="submit" class="Button"><i class="icon-plus"></i> Add</button>
	</div>
	{!! Form::close() !!}
</div>
	

@stop