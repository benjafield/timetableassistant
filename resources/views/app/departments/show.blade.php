@extends('layouts.app')
@section('title', 'Department: ' . $department->name)

@section('content')

<header class="Header__content">
	<h1>Department: {{ $department->name }}</h1>
	<div class="Tools">
		<a href="{{ url('departments/' . $department->id . '/edit') }}"><i class="icon-pencil"></i> Edit</a>
	</div>
</header>

@stop