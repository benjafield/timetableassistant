@extends('layouts.app')
@section('title', 'Create Department')

@section('content')

<header class="Header__content">
	<h1>Create Department</h1>
	<div class="Tools">
		<a href="{{ url('departments/import') }}"><i class="icon-upload2"></i> Import</a>
	</div>
</header>

{!! Form::open(['url' => 'departments']) !!}

@include('partials.forms.department', ['buttonText' => 'Create'])

{!! Form::close() !!}

@stop