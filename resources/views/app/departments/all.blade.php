@extends('layouts.app')
@section('title', ((isset($trash) && $trash) ? 'Deleted Departments' : 'Departments'))

@section('content')

<header class="Header__content">
	<h1>{!! ((isset($trash) && $trash) ? '<a href="'. url('departments') .'"><i class="icon-arrow-left rounded"></i></a> Deleted ' : '') !!}Departments</h1>
	<div class="Tools">
		<a href="{{ url('departments/create') }}"><i class="icon-plus"></i> New</a>
		<a href="{{ url('departments/import') }}"><i class="icon-upload2"></i> Import</a>
		@if (!isset($trash) || !$trash)
		<a href="{{ url('departments/trash') }}"><i class="icon-trash"></i> Deleted</a>
		@endif
	</div>
</header>

<table>
	<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Created</th>
			<th><i class="icon-cog"></i></th>
		</tr>
	</thead>
	<tbody>
	@foreach($departments as $department)
		<tr>
			<td>{{ $department->id }}</td>
			<td><a href="{{ url('departments/' . $department->id) }}">{{ $department->name }}</a></td>
			<td>{{ $department->created_at->diffForHumans() }}</td>
			<td>
				@include('partials.forms.delete', ['id' => $department->id, 'resource' => 'departments', 'trash' => ((isset($trash) && $trash) ? TRUE : FALSE)])
			</td>
		</tr>
	@endforeach
	</tbody>
</table>

@stop