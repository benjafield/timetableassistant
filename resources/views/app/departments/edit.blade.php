@extends('layouts.app')
@section('title', 'Edit Department')

@section('content')

<header class="Header__content">
	<h1>Edit Department: {{ $department->name }}</h1>
</header>

{!! Form::model($department, ['url' => 'departments/' . $department->id, 'method' => 'PUT']) !!}

@include('partials.forms.department', ['buttonText' => 'Edit'])

{!! Form::close() !!}

@stop