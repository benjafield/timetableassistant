@extends('layouts.app')
@section('title', 'Edit Allocation')

@section('content')

<header class="Header__content">
	<h1>Edit Allocation: {{ $allocation->module()->first()->name }}</h1>
</header>

{!! Form::model($allocation, ['url' => 'allocations/' . $allocation->id, 'method' => 'PUT']) !!}
@include('partials.forms.allocationForm', ['buttonText' => 'Edit', 'withRoom' => true])
{!! Form::close() !!}

@stop