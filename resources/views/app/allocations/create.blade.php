@extends('layouts.app')
@section('title', 'Create a Session')

@section('content')

@if(isset($room))
<header class="Header__content">
	<h1><a href="{{ url('rooms/' . $room->id) }}"><i class="icon-arrow-left rounded"></i></a>New Session for {{ $room->label }}</h1>
</header>

{!! Form::open(['url' => 'rooms/' . $room->id . '/sessions']) !!}
@else

<header class="Header__content">
	<h1><a href="{{ url('semesters/' . $semester->id) }}"><i class="icon-arrow-left rounded"></i></a> New Session in <strong>"{{ $semester->label }}"</strong></h1>
</header>

@endif

@include('partials.forms.allocationForm', ['buttonText' => 'Create', 'withRoom' => ((isset($room)) ? true : false)])

{!! Form::close() !!}

@stop