@extends('layouts.app')
@section('title', 'Semesters')

@section('content')

<header class="Header__content">
	<h1>Semesters</h1>
	<div class="Tools">
		<a href="{{ url('semesters/create') }}"><i class="icon-plus"></i> New</a>
	</div>
</header>

<table>
	<thead>
		<tr>
			<th>#</th>
			<th>Semester</th>
			<th>Starts</th>
			<th>Ends</th>
			<th>Created</th>
			<th><i class="icon-cog"></i></th>
		</tr>
	</thead>
	<tbody>
	@foreach($semesters as $semester)
		<tr>
			<td>{{ $semester->id }}</td>
			<td><a href="{{ url('semesters/' . $semester->id) }}">{{ $semester->label }}</a></td>
			<td>{{ $semester->start_date->format('d/m/Y') }}</td>
			<td>{{ $semester->end_date->format('d/m/Y') }}</td>
			<td>{{ $semester->created_at->diffForHumans() }}</td>
			<td>
				{!! Form::open(['url' => 'semesters/' . $semester->id, 'method' => 'DELETE']) !!}
				<button class="Button --small --blend" type="submit"><i class="icon-trash"></i></button>
				{!! Form::close() !!}
			</td>
		</tr>
	@endforeach
	</tbody>
</table>

@stop