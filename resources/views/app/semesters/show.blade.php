@extends('layouts.app')
@section('title', 'Semester: ' . $semester->label)

@section('content')

<header class="Header__content">
	<h1>{{ $semester->label }}</h1>
	<div class="Tools">
		<a href="{{ url('semesters/' . $semester->id . '/edit') }}"><i class="icon-pencil"></i> Edit</a>
		<a href="{{ url('semesters/' . $semester->id . '/allocations/create') }}"><i class="icon-alarm-add2"></i> New Session</a>
	</div>
</header>

@for($i = 1; $i < 6; $i++)
<div class="Timetable --extra-top">
	{!! Bertie::drawRoomTimetableByDay($i, $semester->id) !!}
</div>
@endfor

@stop