@extends('layouts.app')
@section('title', 'New Semester')

@section('content')

<header class="Header__content">
	<h1>New Semester</h1>
</header>

{!! Form::open(['url' => 'semesters']) !!}

@include('partials.forms.semester', ['buttonText' => 'Create'])

{!! Form::close() !!}

@stop