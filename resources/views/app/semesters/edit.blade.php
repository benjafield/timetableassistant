@extends('layouts.app')
@section('title', 'Edit Semester: ' . $semester->label)

@section('content')

<header class="Header__content">
	<h1>Edit Semester: {{ $semester->label }}</h1>
</header>

{!! Form::model($semester, ['url' => 'semesters/' . $semester->id, 'method' => 'PUT']) !!}

@include('partials.forms.semester', ['buttonText' => 'Edit', 'resource' => 'semester'])

{!! Form::close() !!}

@stop