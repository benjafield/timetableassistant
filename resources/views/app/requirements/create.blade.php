@extends('layouts.app')
@section('title', 'Create Requirement')

@section('content')

<header class="Header__content">
	<h1>Create a Requirement</h1>
</header>

{!! Form::open(['url' => 'requirements']) !!}
@include('partials.forms.requirementForm', ['buttonText' => 'Create'])
{!! Form::close() !!}

@stop