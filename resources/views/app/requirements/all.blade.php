@extends('layouts.app')
@section('title', 'Requirements')

@section('content')

<header class="Header__content">
	<h1>Requirements</h1>
	<div class="Tools">
		<a href="{{ url('requirements/create') }}"><i class="icon-plus"></i> New Requirement</a>
	</div>
</header>

<table>
	<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th><i class="icon-cog"></i></th>
		</tr>
	</thead>
	<tbody>
		@foreach($requirements as $requirement)
		<tr>
			<td>{{ $requirement->id }}</td>
			<td>{{ $requirement->name }}</td>
			<td>
				<a href="{{ url('requirements/' . $requirement->id) }}"><i class="icon-pencil"></i></a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

@stop