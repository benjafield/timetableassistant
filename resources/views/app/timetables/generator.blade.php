@extends('layouts.app')
@section('title', 'Generate a Timetable')

@section('content')

{!! Form::open(['url' => 'timetables/generator/1']) !!}

<header class="Header__content">
	<h1>Timetable Generator</h1>
	<div class="Tools">
		<button type="submit" class="Button">Next Step <i class="icon-arrow-right"></i></button>
	</div>
</header>

<p>
	Set the parameters and constraint rules for the timetable.
</p>

<hr>

<div class="Modular-form">
	<div class="Row">
		<div class="sm12">
			<div class="Panel --n-p">
				<h2 class="Panel__heading">General Settings</h2>

				<div class="inputs">
					{!! Form::label('semesterName', 'Semester Name') !!}
					{!! Form::text('semesterName', old('semesterName'), ['id' => 'semesterName', 'placeholder' => 'Semester Name']) !!}
				</div>

			</div>
		</div>
	</div>
	<div class="Row">
		<div class="sm12">

			<div class="Panel --n-p">
				<h2 class="Panel__heading">Time Settings</h2>

				<div class="inputs">
					{!! Form::label('minTime_hour', 'Start Time') !!}

					{!! Form::text('minTime_hour', old('minTime_hour', '09'), ['id' => 'minTime_hour', 'placeholder' => 'HH', 'class' => 'Time__input']) !!}
					:
					{!! Form::text('minTime_minute', old('minTime_minute', '00'), ['id' => 'minTime_minute', 'placeholder' => 'MM', 'class' => 'Time__input']) !!}
				</div>

				<div class="inputs">
					{!! Form::label('maxTime_hour', 'End Time') !!}

					{!! Form::text('maxTime_hour', old('maxTime_hour', '18'), ['id' => 'maxTime_hour', 'placeholder' => 'HH', 'class' => 'Time__input']) !!}
					:
					{!! Form::text('maxTime_minute', old('maxTime_minute', '00'), ['id' => 'maxTime_minute', 'placeholder' => 'MM', 'class' => 'Time__input']) !!}
				</div>

				<div class="inputs">
					{!! Form::label('chosenDays', 'Days to include') !!}
					{!! Form::select('chosenDays', [1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday'], old('chosenDays'), ['id' => 'chosenDays', 'multiple' => 'true']) !!}
				</div>

			</div>
		</div>
	</div>

	<div class="Row">
		<div class="sm12">
			<div class="Panel --n-p">
				<h2 class="Panel__heading">Modules</h2>

				<div class="inputs">
					{!! Form::label('chosenModules', 'Choose the modules that should be included in the timetable.') !!}
					{!! Form::select('chosenModules', \App\Module::formSelect(), old('chosenModules'), ['id' => 'chosenModules', 'multiple' => 'true']) !!}
				</div>

			</div>
		</div>
	</div>
</div>

{!! Form::close() !!}

@stop