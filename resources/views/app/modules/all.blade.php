@extends('layouts.app')
@section('title', ((isset($trash) && $trash) ? 'Deleted Modules' : 'Modules'))

@section('content')

<header class="Header__content">
	<h1>{!! ((isset($trash) && $trash) ? '<a href="'. url('modules') .'"><i class="icon-arrow-left rounded"></i></a> Deleted ' : '') !!}Modules</h1>
	<div class="Tools">
		<a href="{{ url('modules/create') }}"><i class="icon-plus"></i> New</a>
		<a href="{{ url('modules/import') }}"><i class="icon-upload2"></i> Import</a>
		@if (!isset($trash) || !$trash)
		<a href="{{ url('modules/trash') }}"><i class="icon-trash"></i> Deleted</a>
		@endif
	</div>
</header>

<table>
	<thead>
		<tr>
			<th>#</th>
			<th>Code</th>
			<th>Name</th>
			<th>Created</th>
			<th><i class="icon-cog"></i></th>
		</tr>
	</thead>
	<tbody>
	@foreach($modules as $module)
		<tr>
			<td>{{ $module->id }}</td>
			<td>{{ $module->code }}</td>
			<td><a href="{{ url('modules/' . $module->id) }}">{{ $module->name }}</a></td>
			<td>{{ $module->created_at->diffForHumans() }}</td>
			<td>
				@include('partials.forms.delete', ['id' => $module->id, 'resource' => 'modules', 'trash' => ((isset($trash) && $trash) ? TRUE : FALSE)])
			</td>
		</tr>
	@endforeach
	</tbody>
</table>

@stop