@extends('layouts.app')
@section('title', 'Edit Module: ' . $module->name)

@section('content')

<header class="Header__content">
	<h1>Edit Module: {{ $module->name }}</h1>
</header>

{!! Form::model($module, ['url' => 'modules/' . $module->id, 'method' => 'PUT']) !!}

@include('partials.forms.coursemodule', ['buttonText' => 'Edit', 'resource' => 'modules'])

{!! Form::close() !!}

<hr>

<h2>Requirements</h2>

<div class="Row">
	<table>
		<thead>
			<tr>
				<th>Name</th>
				<th><i class="icon-cog"></i></th>
			</tr>
		</thead>
		<tbody>
		@foreach($module->requirements as $requirement)
			<tr>
				<td>{{ $requirement->name }}</td>
				<td>
					{!! Form::open(['url' => 'modules/' . $module->id . '/requirements/' . $requirement->id, 'method' => 'DELETE']) !!}
					<button class="Button --small --blend" type="submit"><i class="icon-trash"></i></button>
					{!! Form::close() !!}
				</td>
			</tr>
		@endforeach
		</tbody>
	</table>
</div>

<div class="Row">
	{!! Form::open(['url' => 'modules/' . $module->id . '/requirements']) !!}
	<div class="inputs">
		{!! Form::label('requirement_id', 'Apply a requirement') !!}
		{!! Form::select('requirement_id', \App\Requirement::formSelect(), null, ['placeholder' => 'Please Select...']) !!}
	</div>
	<div class="inputs">
		<button type="submit" class="Button"><i class="icon-plus"></i> Add</button>
	</div>
	{!! Form::close() !!}
</div>

@stop