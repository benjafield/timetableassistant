@extends('layouts.app')
@section('title', 'Module: ' . $module->name)

@section('content')

<header class="Header__content">
	<h1>Module: {{ $module->name }}</h1>
	<div class="Tools">
		<a href="{{ url('modules/' . $module->id . '/edit') }}"><i class="icon-pencil"></i> Edit</a>
	</div>
</header>

@stop