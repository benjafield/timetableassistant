@extends('layouts.app')
@section('title', 'New Module')

@section('content')

<header class="Header__content">
	<h1>New Module</h1>
	<div class="Tools">
		<a href="{{ url('modules/import') }}"><i class="icon-upload2"></i> Import</a>
	</div>
</header>

{!! Form::open(['url' => 'modules']) !!}

@include('partials.forms.coursemodule', ['buttonText' => 'Create', 'resource' => 'modules'])

{!! Form::close() !!}

@stop