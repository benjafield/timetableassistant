@extends('layouts.app')
@section('title', 'Course: ' . $course->name)

@section('content')

<header class="Header__content">
	<h1>Course: {{ $course->name }}</h1>
	<div class="Tools">
		<a href="{{ url('courses/' . $course->id . '/edit') }}"><i class="icon-pencil"></i> Edit</a>
		<a href="{{ url('courses/' . $course->id . '/modules/create') }}"><i class="icon-graduation-hat"></i> New Module</a>
	</div>
</header>

@stop