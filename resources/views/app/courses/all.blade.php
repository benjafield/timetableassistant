@extends('layouts.app')
@section('title', ((isset($trash) && $trash) ? 'Deleted Courses' : 'Courses'))

@section('content')

<header class="Header__content">
	<h1>{!! ((isset($trash) && $trash) ? '<a href="'. url('courses') .'"><i class="icon-arrow-left rounded"></i></a> Deleted ' : '') !!}Courses</h1>
	<div class="Tools">
		<a href="{{ url('courses/create') }}"><i class="icon-plus"></i> New</a>
		<a href="{{ url('courses/import') }}"><i class="icon-upload2"></i> Import</a>
		@if (!isset($trash) || !$trash)
		<a href="{{ url('courses/trash') }}"><i class="icon-trash"></i> Deleted</a>
		@endif
	</div>
</header>

<table>
	<thead>
		<tr>
			<th>#</th>
			<th>Course</th>
			<th>Created</th>
			<th><i class="icon-cog"></i></th>
		</tr>
	</thead>
	<tbody>
	@foreach($courses as $course)
		<tr>
			<td>{{ $course->id }}</td>
			<td><a href="{{ url('courses/' . $course->id) }}">{{ $course->name }}</a></td>
			<td>{{ $course->created_at->diffForHumans() }}</td>
			<td>
				@include('partials.forms.delete', ['id' => $course->id, 'resource' => 'courses', 'trash' => ((isset($trash) && $trash) ? TRUE : FALSE)])
			</td>
		</tr>
	@endforeach
	</tbody>
</table>

@stop