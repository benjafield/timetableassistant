@extends('layouts.app')
@section('title', 'Edit Course: ' . $course->name)

@section('content')

<header class="Header__content">
	<h1>Edit Course: {{ $course->name }}</h1>
</header>

{!! Form::model($course, ['url' => 'courses/' . $course->id, 'method' => 'PUT']) !!}

@include('partials.forms.coursemodule', ['buttonText' => 'Edit', 'resource' => 'courses'])

{!! Form::close() !!}

@stop