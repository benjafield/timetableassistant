@extends('layouts.app')
@section('title', 'Courses')

@section('content')

<header class="Header__content">
	<h1>New Course</h1>
	<div class="Tools">
		<a href="{{ url('courses/import') }}"><i class="icon-upload2"></i> Import</a>
	</div>
</header>

{!! Form::open(['url' => 'courses']) !!}

@include('partials.forms.coursemodule', ['buttonText' => 'Create', 'resource' => 'courses'])

{!! Form::close() !!}

@stop