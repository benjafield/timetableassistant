@extends('layouts.app')
@section('title', auth()->user()->firstname . ' ' . auth()->user()->lastname)

@section('content')

	<h1>{{ Bertie::relativeGreeting() }}, {{ auth()->user()->firstname }}.</h1>

	<div class="Row">
		<div class="sm3">
			<a class="Panel --b-b --hoverable" href="{{ url('modules') }}">
				<span class="Panel__stat">{{ \App\Module::count() }}</span>
				<span class="Panel__stat-label">Modules</span>
			</a>
		</div>
		<div class="sm3">
			<a class="Panel --b-b --hoverable" href="{{ url('courses') }}">
				<span class="Panel__stat">{{ \App\Course::count() }}</span>
				<span class="Panel__stat-label">Courses</span>
			</a>
		</div>
		<div class="sm3">
			<a class="Panel --b-b --hoverable" href="{{ url('departments') }}">
				<span class="Panel__stat">{{ \App\Department::count() }}</span>
				<span class="Panel__stat-label">Departments</span>
			</a>
		</div>
		<div class="sm3">
			<a class="Panel --b-b --hoverable" href="{{ url('rooms') }}">
				<span class="Panel__stat">{{ \App\Room::count() }}</span>
				<span class="Panel__stat-label">Rooms</span>
			</a>
		</div>
	</div>

	<div class="Row">
		<div class="sm12">
			<div class="Panel --bordered">
				<h2>Timetable Generator</h2>
				<p>Using this feature will attempt to automatically generate a timetable for a semester.</p>
				<p>
					<a href="{{ url('timetables/generate') }}" class="Button"><i class="icon-power"></i> Generate</a>
				</p>
			</div>
		</div>
	</div>

	

@stop