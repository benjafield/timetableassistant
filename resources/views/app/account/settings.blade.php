@extends('layouts.app')
@section('title', 'Account Settings')

@section('content')
	
	<header class="Header__content">
		<h1>Account Settings</h1>
	</header>

	{!! Form::model($user, ['url' => 'account/settings', 'method' => 'PUT']) !!}

	@include('partials.forms.validationErrors')
	
	<div class="inputs">
		{!! Form::label('firstname', 'First Name') !!}
		{!! Form::text('firstname', old('firstname'), ['id' => 'firstname', 'placeholder' => 'First Name']) !!}
	</div>

	<div class="inputs">
		{!! Form::label('lastname', 'Last Name') !!}
		{!! Form::text('lastname', old('lastname'), ['id' => 'lastname', 'placeholder' => 'Last Name']) !!}
	</div>

	<div class="inputs">
		{!! Form::label('email', 'Email') !!}
		{!! Form::email('email', old('email'), ['id' => 'email', 'placeholder' => 'Email']) !!}
	</div>

	<hr>

	<div class="inputs">
		{!! Form::label('password', 'New Password') !!}
		{!! Form::password('password', ['id' => 'password', 'placeholder' => 'Password']) !!}
	</div>

	<div class="inputs">
		{!! Form::label('password_confirmation', 'Confirm Password') !!}
		{!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'placeholder' => 'Confirm Password']) !!}
	</div>

	<div class="inputs">
		<button type="submit" class="Button"><i class="icon-check"></i> Save Settings</button>
	</div>

	{!! Form::close() !!}

@stop