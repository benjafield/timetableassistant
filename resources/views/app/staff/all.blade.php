@extends('layouts.app')
@section('title', 'Staff')

@section('content')

<header class="Header__content">
	<h1>Staff</h1>
	<div class="Tools">
		<a href="{{ url('staff/create') }}"><i class="icon-plus"></i> New</a>
	</div>
</header>

<table>
	<thead>
		<tr>
			<th>#</th>
			<th>Name</th>
			<th>Email</th>
			<th>Status</th>
			<th><i class="icon-cog"></i></th>
		</tr>
	</thead>
	<tbody>
		@foreach($staffs as $staff)
		<tr>
			<td>{{ $staff->id }}</td>
			<td><a href="{{ url('staff/' . $staff->id . '/edit') }}">{{ $staff->firstname . ' ' . $staff->lastname }}</a></td>
			<td><a href="mailto:{{ $staff->email }}">{{ $staff->email }}</a></td>
			<td>{{ (($staff->status_id == 1) ? 'Active' : 'Inactive') }}</td>
			<td>
				<a href="{{ url('staff/' . $staff->id . '/edit') }}"><i class="icon-pencil"></i></a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

@stop