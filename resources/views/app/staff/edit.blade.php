@extends('layouts.app')
@section('title', 'Edit: ' . $staff->firstname . ' ' . $staff->lastname)

@section('content')
	
<header class="Header__content">
	<h1>Edit: {{ $staff->firstname . ' ' . $staff->lastname }}</h1>
</header>

{!! Form::model($staff, ['url' => 'staff/' . $staff->id, 'method' => 'PUT']) !!}

@include('partials.forms.staffForm', ['buttonText' => 'Save'])

{!! Form::close() !!}

@stop