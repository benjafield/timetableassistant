@extends('layouts.app')
@section('title', 'Create Staff Member')

@section('content')

<header class="Header__content">
	<h1>Create a Staff Member</h1>
	<div class="Tools">
		<a href="{{ url('staff') }}"><i class="icon-arrow-left"></i> Back</a>
	</div>
</header>

{!! Form::open(['url' => 'staff']) !!}

@include('partials.forms.staffForm', ['buttonText' => 'Create'])

{!! Form::close() !!}

@stop