<header class="Header">
	<a href="{{ url('') }}" class="Logo"><i class="icon-clock"></i> <strong>Timetable</strong>Assistant</a>
	<div class="User">
		<a href="#" data-dropdown="userMenu">{{ auth()->user()->firstname . ' ' . auth()->user()->lastname }} <i class="icon-user avatar"></i><i class="icon-chevron-down"></i></a>
		
		<div class="Dropdown" id="userMenu">
			<ul>
				<li><a href="{{ url('account/settings') }}"><i class="icon-equalizer"></i> Account Settings</a></li>
				<li class="divider"></li>
				<li><a href="{{ url('logout') }}"><i class="icon-power-switch"></i> Logout</a></li>
			</ul>
		</div>
	</div>
</header>

<div class="Sidebar">
	<nav class="Navigation">
		<a href="{{ url('') }}" {!! ((empty(Request::segment(1))) ? 'class="active"' : '') !!}><i class="icon-graph"></i> Dashboard</a>
		<a href="{{ url('rooms') }}" {!! ((Request::segment(1) == 'rooms') ? 'class="active"' : '') !!}><i class="icon-chair"></i> Rooms</a>
		<a href="{{ url('courses') }}" {!! ((Request::segment(1) == 'courses') ? 'class="active"' : '') !!}><i class="icon-graduation-hat"></i> Courses</a>
		<a href="{{ url('modules') }}" {!! ((Request::segment(1) == 'modules') ? 'class="active"' : '') !!}><i class="icon-book2"></i> Modules</a>
		<a href="{{ url('departments') }}" {!! ((Request::segment(1) == 'departments') ? 'class="active"' : '') !!}><i class="icon-library2"></i> Departments</a>
		<a href="{{ url('staff') }}" {!! ((Request::segment(1) == 'staff') ? 'class="active"' : '') !!}><i class="icon-users"></i> Staff</a>
		<a href="{{ url('semesters') }}" {!! ((Request::segment(1) == 'semesters') ? 'class="active"' : '') !!}><i class="icon-calendar-full"></i> Semesters</a>
	</nav>
</div>