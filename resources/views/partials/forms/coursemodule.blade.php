@include('partials.forms.validationErrors')

@if($resource != 'courses')
<div class="inputs">
	{!! Form::label('code', 'Code') !!}
	{!! Form::text('code', old('code'), ['id' => 'code', 'placeholder' => 'Code']) !!}
</div>
@endif

<div class="inputs">
	{!! Form::label('name', 'Name') !!}
	{!! Form::text('name', old('name'), ['id' => 'name', 'placeholder' => 'Name']) !!}
</div>

<div class="inputs">
	{!! Form::label('colour', 'Colour') !!}
	{!! Form::color('colour', old('colour'), ['id' => 'colour', 'placeholder' => 'Colour']) !!}
</div>

@if($resource == 'courses')
<div class="inputs">
	{!! Form::label('department_id', 'Department') !!}
	{!! Form::text('department_id', old('department_id'), ['id' => 'department_id', 'placeholder' => 'Department']) !!}
</div>
@endif

<div class="inputs">
	{!! Form::label('leader_user_id', 'Leader') !!}
	{!! Form::select('leader_user_id', \App\User::formSelect(), old('leader_user_id'), ['id' => 'leader_user_id']) !!}
</div>

<div class="inputs">
	<button type="submit" class="Button"><i class="icon-check"></i> {{ $buttonText }} {{ ucfirst(rtrim($resource, 's')) }}</button>
</div>