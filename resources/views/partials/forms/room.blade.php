@include('partials.forms.validationErrors')

<div class="inputs">
	{!! Form::label('label', 'Name') !!}
	{!! Form::text('label', old('label'), ['id' => 'label', 'placeholder' => 'Name of Room']) !!}
</div>

<div class="inputs">
	{!! Form::label('max_students', 'Maximum Students') !!}
	{!! Form::number('max_students', old('max_students'), ['id' => 'max_students', 'placeholder' => 'Maximum Students']) !!}
</div>

<div class="inputs">
	<button type="submit" class="Button"><i class="icon-check"></i> {{ $buttonText }} Room</button>
</div>