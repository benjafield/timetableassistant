@include('partials.forms.validationErrors')

<div class="inputs">
	{!! Form::label('name', 'Name') !!}
	{!! Form::text('name', old('name'), ['id' => 'Name', 'placeholder' => 'Name']) !!}
</div>

<div class="inputs">
	<button class="Button" type="submit"><i class="icon-check"></i> {{ $buttonText }} Requirement</button>
</div>