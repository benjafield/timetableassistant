@include('partials.forms.validationErrors')

<div class="inputs">
	{!! Form::label('firstname', 'Firstname') !!}
	{!! Form::text('firstname', old('firstname'), ['id' => 'firstname', 'placeholder' => 'Firstname']) !!}
</div>

<div class="inputs">
	{!! Form::label('lastname', 'Lastname') !!}
	{!! Form::text('lastname', old('lastname'), ['id' => 'lastname', 'placeholder' => 'Lastname']) !!}
</div>

<div class="inputs">
	{!! Form::label('email', 'Email') !!}
	{!! Form::email('email', old('email'), ['id' => 'email', 'placeholder' => 'Email']) !!}
</div>

<hr>

<div class="inputs">
	{!! Form::label('password', 'Password') !!}
	{!! Form::password('password', ['id' => 'password', 'placeholder' => 'Password']) !!}
</div>

<div class="inputs">
	{!! Form::label('password_confirmation', 'Confirm Password') !!}
	{!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'placeholder' => 'Confirm Password']) !!}
</div>

<hr>

<div class="inputs">
	{!! Form::label('status_id', 'Status') !!}
	{!! Form::select('status_id', [0 => 'Inactive', 1 => 'Active'], old('status_id')) !!}
</div>

<div class="inputs">
	<button type="submit" class="Button"><i class="icon-check"></i> {{ $buttonText }} Staff Member</button>
</div>