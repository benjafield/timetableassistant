@include('partials.forms.validationErrors')

<div class="inputs">
	{!! Form::label('name', 'Name') !!}
	{!! Form::text('name', old('name'), ['id' => 'name', 'placeholder' => 'Name of department']) !!}
</div>

<div class="inputs">
	{!! Form::label('leader_user_id', 'Leader') !!}
	{!! Form::select('leader_user_id', \App\User::formSelect(), old('leader_user_id'), ['id' => 'leader_user_id']) !!}
</div>

<div class="inputs">
	<button type="submit" class="Button"><i class="icon-check"></i> {{ $buttonText }} Department</button>
</div>