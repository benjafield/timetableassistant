@include('partials.forms.validationErrors')

<div class="inputs">
	{!! Form::label('label', 'Name') !!}
	{!! Form::text('label', old('label'), ['id' => 'label', 'placeholder' => 'Name of Semester']) !!}
</div>

<div class="inputs">
	{!! Form::label('start_date', 'Start Date') !!}
	{!! Form::date('start_date', old('start_date'), ['id' => 'start_date', 'placeholder' => 'Start Date']) !!}
</div>

<div class="inputs">
	{!! Form::label('end_date', 'End Date') !!}
	{!! Form::date('end_date', old('end_date'), ['id' => 'end_date', 'placeholder' => 'End Date']) !!}
</div>

<div class="inputs">
	<button type="submit" class="Button"><i class="icon-check"></i> {{ $buttonText }} Semester</button>
</div>