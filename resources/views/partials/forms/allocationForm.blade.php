@include('partials.forms.validationErrors')

<div class="inputs">
	{!! Form::label('module_id', 'Module') !!}
	{!! Form::select('module_id', \App\Module::formSelect(), old('module_id')) !!}
	</select>
</div>

<div class="inputs">
	{!! Form::label('semester_id', 'Semester') !!}
	{!! Form::select('semester_id', \App\Semester::formSelect()['data'], old('semester_id', \App\Semester::formSelect()['current'])) !!}
</div>

@if(!$withRoom)
<div class="inputs">
	{!! Form::label('room_id', 'Room') !!}
	{!! Form::select('room_id', \App\Room::formSelect(), old('room_id')) !!}
</div>
@endif

<div class="inputs">
	{!! Form::label('staff_user_id', 'Staff') !!}
	{!! Form::select('staff_user_id', \App\User::formSelect(), old('staff_user_id')) !!}
</div>

<div class="inputs">
	{!! Form::label('day_id', 'Day') !!}
	{!! Form::select('day_id', [\Carbon\Carbon::MONDAY => 'Monday', \Carbon\Carbon::TUESDAY => 'Tuesday', \Carbon\Carbon::WEDNESDAY => 'Wednesday', \Carbon\Carbon::THURSDAY => 'Thursday', \Carbon\Carbon::FRIDAY => 'Friday']) !!}
</div>

<div class="inputs">
	{!! Form::label('starts_at', 'Starts') !!}
	{!! Form::text('starts_at', old('starts_at'), ['placeholder' => 'HH:MM:SS']) !!}
</div>

<div class="inputs">
	{!! Form::label('ends_at', 'Ends') !!}
	{!! Form::text('ends_at', old('ends_at'), ['placeholder' => 'HH:MM:SS']) !!}
</div>

<div class="inputs">
	{!! Form::label('notes', 'Notes') !!}
	{!! Form::textarea('notes', old('notes'), ['placeholder' => 'Notes', 'rows' => 5]) !!}
</div>

<div class="inputs">
	<button type="submit" class="Button"><i class="icon-check"></i> {{ $buttonText }} Session</button>
</div>