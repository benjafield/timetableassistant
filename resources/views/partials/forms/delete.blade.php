{!! Form::open(['url' => $resource . '/' . $id, 'method' => 'DELETE', 'id' => 'delete' . ucfirst($resource) . $id]) !!}
@if (!isset($trash) || !$trash)
	<a href="{{ url($resource . '/' . $id . '/edit') }}" class="Button --small --blend"><i class="icon-pencil"></i></a>
@else
	<a href="{{ url($resource . '/' . $id . '/restore') }}" class="Button --small --blend"><i class="icon-undo2"></i></a>
	{!! Form::hidden('permanent', 1) !!}
@endif
<button type="submit" class="Button --small --blend"><i class="icon-trash"></i></button>
{!! Form::close() !!}