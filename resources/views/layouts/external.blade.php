<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="author" content="Charlie Benjafield">
	<title>@yield('title') / Timetable Assistant</title>
	<link href="{{ url('css/app.css') }}" rel="stylesheet">
</head>
<body>
	
	<header class="Header">
		<a href="{{ url('') }}" class="Logo"><i class="icon-clock"></i> <strong>Timetable</strong>Assistant</a>
		
	</header>

	<main class="Main --external">
		@yield('content')
	</main>

</body>
</html>