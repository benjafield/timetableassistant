<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="author" content="Charlie Benjafield">
	<title>@yield('title') / Timetable Assistant</title>
	<link href="{{ url('css/app.css') }}" rel="stylesheet">
</head>
<body>
	
	@include('partials.templates.appHeader')
	
	<main class="Main">
		@yield('content')
	</main>

	@include('partials.templates.appFooter')

	<script type="text/javascript" src="{{ url('js/all.js') }}"></script>

</body>
</html>