<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="author" content="Charlie Benjafield">
	<title>@yield('title') / Timetable Assistant</title>
	<link href="{{ url('css/app.css') }}" rel="stylesheet">
</head>
<body class="auth">
	
	@include('partials.templates.authHeader')
	
	@yield('content')

	@include('partials.templates.authFooter')

</body>
</html>