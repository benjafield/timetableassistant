@extends('layouts.auth')
@section('title', 'Login')

@section('content')

<div class="form login">
	<div class="form-header logo">
		<i class="fa fa-clock-o"></i>
	</div>

	@include('partials.forms.validationErrors')

	<div class="Login">

		{!! Form::open(['url' => 'login', 'id' => 'loginForm']) !!}
		<div class="Login__header">
			<i class="icon-clock"></i>
		</div>
		
		<div class="Auth__input">
			{!! Form::label('email', 'Email', ['class' => 'sr']) !!}
			{!! Form::email('email', old('email'), ['id' => 'email', 'placeholder' => 'Email Address']) !!}
		</div>

		<div class="Auth__input">
			{!! Form::label('password', 'Password', ['class' => 'sr']) !!}
			{!! Form::password('password', ['id' => 'password', 'placeholder' => 'Password']) !!}
		</div>

		<div class="Auth__input --options Row">
			<div class="sm6 txtl">
				{!! Form::checkbox('remember', 1) !!} Remember
			</div>
			<div class="sm6 txtr">
				<a href="{{ url('password/email') }}">Forgot Password?</a>
			</div>
		</div>

		<div class="Auth__button">
			<button type="submit" class="Button --login --expanded"><i class="icon-check"></i> Login</button>
		</div>

		<div class="Login__footer">
			<a href="{{ url('register') }}">Not have an account? Register</a>
		</div>
		{!! Form::close() !!}

		<p class="Divider">
			<span>OR</span>
		</p>

		<div class="Auth__input">
			{!! Form::open(['url' => 'search', 'method' => 'GET', 'autocomplete' => 'off']) !!}
			{!! Form::text('q', '', ['placeholder' => 'Search room and hit enter...']) !!}
			{!! Form::close() !!}
		</div>

	</div>
</div>

@stop