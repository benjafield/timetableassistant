@extends('layouts.auth')
@section('title', 'Register')

@section('content')

<div class="form register">
	
	<div class="Register">
		{!! Form::open(['url' => 'register', 'id' => 'registerForm']) !!}

			<div class="Register__header">
				<h1>Register an account</h1>
			</div>
			<div class="form-body">

				@include('partials.forms.validationErrors')

				<div class="Auth__input">
					{!! Form::label('firstname', 'First name') !!}
					{!! Form::text('firstname', old('firstname'), ['id' => 'firstname', 'placeholder' => 'First name']) !!}
				</div>

				<div class="Auth__input">
					{!! Form::label('lastname', 'Last name') !!}
					{!! Form::text('lastname', old('lastname'), ['id' => 'lastname', 'placeholder' => 'Last name']) !!}
				</div>
				
				<div class="Auth__input">
					{!! Form::label('email', 'Email Address') !!}
					{!! Form::email('email', old('email'), ['id' => 'email', 'placeholder' => 'Email Address']) !!}
				</div>

				<div class="Auth__input">
					{!! Form::label('password', 'Password') !!}
					{!! Form::password('password', ['id' => 'password', 'placeholder' => 'Password']) !!}
				</div>

				<div class="Auth__input">
					{!! Form::label('password_confirmation', 'Confirm Password') !!}
					{!! Form::password('password_confirmation', ['id' => 'password_confirmation', 'placeholder' => 'Confirm Password']) !!}
				</div>

				<div class="Auth__button">
					<button type="submit" class="Button --login --expanded"><i class="icon-check"></i> Register</button>
				</div>

			</div>
			<div class="Register__footer">
				<a href="{{ url('login') }}">&larr; Back to Login</a>
			</div>
		{!! Form::close() !!}
	</div>

</div>

@stop