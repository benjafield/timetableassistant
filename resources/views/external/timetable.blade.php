@extends('layouts.external')
@section('title', 'Timetable: ' . $room->label)

@section('content')

	<h1>Timetable for <strong>{{ $room->label }}</strong></h1>
	<hr>

	<div class="Timetable">
		{!! Bertie::drawRoomTimetable($room->id, $allocations) !!}
	</div>

@stop