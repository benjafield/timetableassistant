@extends('layouts.external')
@section('title', $title)

@section('content')

@if(!empty($term))
	<h1>Rooms matching <strong>"{{ $term }}"</strong></h1>
@else
	<h1>Search for a room</h1>
@endif

<hr>

@if(empty($term))

@else

	@if(count($results) > 0)
		<ul class="Search__results">
			@foreach($results as $room)
			<li>
				<a href="{{ url('timetable/' . $room->label) }}">
					{{ $room->label }}
				</a>
			</li>
			@endforeach
		</ul>
	@else
		<p>No rooms were found for that search.</p>
	@endif

@endif

@stop