<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('room_requirement', function (Blueprint $table) {
            $table->integer('room_id')->unsigned();
            $table->integer('requirement_id')->unsigned();
            
            /**
             * Set the primary key to a compound of both fields.
             */
            $table->primary(['room_id', 'requirement_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('room_requirements');
    }
}
