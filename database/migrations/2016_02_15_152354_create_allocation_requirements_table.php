<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllocationRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allocation_requirement', function (Blueprint $table) {
            $table->integer('allocation_id')->unsigned();
            $table->integer('requirement_id')->unsigned();

            $table->primary(['allocation_id', 'requirement_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('allocation_requirements');
    }
}
