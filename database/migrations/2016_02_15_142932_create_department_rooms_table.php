<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_room', function (Blueprint $table) {
            $table->integer('department_id')->unsigned();
            $table->integer('room_id')->unsigned();

            /**
             * Set the primary key as a compound.
             */
            $table->primary(['department_id', 'room_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('department_rooms');
    }
}
