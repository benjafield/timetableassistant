<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAllocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allocations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('semester_id')->unsigned();
            $table->integer('module_id')->unsigned();
            $table->integer('room_id')->unsigned();
            $table->integer('day_id')->unsigned();
            $table->time('starts_at');
            $table->time('ends_at');
            $table->integer('user_id')->unsigned();
            $table->integer('staff_user_id')->unsigned();
            $table->text('notes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('allocations');
    }
}
