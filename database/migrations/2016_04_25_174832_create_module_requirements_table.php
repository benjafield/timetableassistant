<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModuleRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_requirement', function (Blueprint $table) {
            $table->integer('module_id')->unsigned();
            $table->integer('requirement_id')->unsigned();
            $table->primary(['module_id', 'requirement_id']);
            $table->foreign('module_id')
                  ->references('id')
                  ->on('modules')
                  ->onDelete('cascade');

            $table->foreign('requirement_id')
                  ->references('id')
                  ->on('requirements')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_requirement');
    }
}
