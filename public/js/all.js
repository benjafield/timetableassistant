// Scripts

function _(element) {
	return document.querySelector(element);
}

function toggleClass(el, className) {
	if (el.classList.contains(className)){
		el.classList.remove(className);
	} else {
		el.classList.add(className);
	}
}

_('[data-dropdown]').addEventListener('click', function(e) {
	e.preventDefault();
	var target = '#' + this.attributes["data-dropdown"].value;
	toggleClass(_(target), 'active');
});
//# sourceMappingURL=all.js.map
